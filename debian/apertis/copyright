Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: The XZ Utils authors and contributors
License: 0BSD

Files: INSTALL.generic
Copyright: 1994-1996, 1999-2002, 2004-2016, Free Software
License: FSFAP

Files: aclocal.m4
Copyright: 1996-2024, Free Software Foundation, Inc.
License: FSFULLR

Files: build-aux/*
Copyright: 1996-2024, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: build-aux/config.rpath
Copyright: 1996-2024, Free Software Foundation, Inc.
License: FSFULLR

Files: build-aux/install-sh
Copyright: 1994, X Consortium
License: X11

Files: build-aux/ltmain.sh
Copyright: 1996-2019, 2021-2024, Free Software Foundation, Inc.
License: (Expat or GPL-2+) with Libtool exception

Files: build-aux/manconv.sh
 build-aux/version.sh
Copyright: no-info-found
License: 0BSD

Files: configure
Copyright: 1992-1996, 1998-2017, 2020-2023, Free Software Foundation
License: FSFUL

Files: debian/*
Copyright: 2009-2012, Jonathan Nieder
License: PD-debian

Files: extra/*
Copyright: 2006, Timo Lindfors
License: GPL-2+

Files: extra/7z2lzma/*
Copyright: no-info-found
License: 0BSD

Files: lib/*
Copyright: 1987-2023, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: lib/Makefile.am
Copyright: 2004-2007, Free Software Foundation, Inc.
License: GPL-2+

Files: m4/*
Copyright: 1996-2024, Free Software Foundation, Inc.
License: FSFULLR

Files: m4/ax_pthread.m4
Copyright: 2019, Marc Stevens <marc.stevens@cwi.nl>
 2011, Daniel Richard G. <skunk@iSKUNK.ORG>
 2008, Steven G. Johnson <stevenj@alum.mit.edu>
License: GPL-3+ with Autoconf-2.0~Archive exception

Files: m4/gettext.m4
 m4/intlmacosx.m4
 m4/progtest.m4
Copyright: 1995-2024, Free Software Foundation, Inc.
License: FSFULLR or GPL or LGPL

Files: m4/iconv.m4
 m4/ltversion.m4
Copyright: 2000-2002, 2004, 2007-2024, Free Software Foundation
License: FSFULLR

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2019, 2021-2024, Free Software
License: (FSFULLR or GPL-2+) with Libtool exception

Files: m4/ltoptions.m4
 m4/lt~obsolete.m4
Copyright: 2004, 2005, 2007-2009, 2011-2019, 2021-2024, Free
License: FSFULLR

Files: m4/ltsugar.m4
Copyright: 2004, 2005, 2007, 2008, 2011-2019, 2021-2024, Free Software
License: FSFULLR

Files: m4/nls.m4
Copyright: 1995-2003, 2005, 2006, 2008-2014, 2016, 2019-2024, Free
License: FSFULLR or GPL or LGPL

Files: m4/po.m4
Copyright: 1995-2014, 2016, 2018-2022, 2024, Free Software Foundation
License: FSFULLR or GPL or LGPL

Files: m4/tuklib_common.m4
 m4/tuklib_cpucores.m4
 m4/tuklib_integer.m4
 m4/tuklib_mbstr.m4
 m4/tuklib_physmem.m4
 m4/tuklib_progname.m4
Copyright: no-info-found
License: 0BSD

Files: po4a/fr.po
Copyright: 2021, Debian French l10n team <debian-l10n-french@lists.debian.org>
License: public-domain

Files: po4a/man/fr/*
Copyright: 2021, Debian French l10n team <debian-l10n-french@lists.debian.org>
License: 0BSD

Files: po4a/pt_BR.po
Copyright: no-info-found
License: public-domain

Files: src/scripts/xzdiff.in
 src/scripts/xzgrep.in
 src/scripts/xzmore.in
Copyright: 1998, 2001, 2002, 2006, 2007, Free Software Foundation
 1992, 1993, Jean-loup Gailly
License: GPL-2+

Files: src/scripts/xzless.in
Copyright: 1998, 2002, 2006, 2007, Free Software Foundation
License: GPL-2+

Files: Makefile.in debug/Makefile.in src/Makefile.in src/liblzma/Makefile.in src/liblzma/api/Makefile.in src/lzmainfo/Makefile.in src/xz/Makefile.in src/xzdec/Makefile.in tests/Makefile.in
Copyright: Lasse Collin
 Igor Pavlov
 Andrew Dudman
 Chenxi Mao
 Ilya Kurdyukov
 Jonathan Nieder
 Joachim Henke
 Hans Jansen
 Maksym Vatsyk
 Michał Górny
 Sebastian Andrzej Siewior
 Wei Dai
License: 0BSD

Files: lib/Makefile.in src/scripts/Makefile.in
Copyright: © 1993, Jean-loup Gailly
 © 1989-1994, 1996-1999, 2001-2007, Free Software Foundation, Inc.
 © 2006 Timo Lindfors
 2005, Charles Levert
 2005, 2009, Lasse Collin
 2009, Andrew Dudman
License: GPL-2+
